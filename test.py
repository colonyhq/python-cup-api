from cup.client import CUPClient

client = CUPClient(
    access_token='75f8f43c62ea5e021f68920598d46bf371df68fb'
)

data = {
    'colony_campaign_id': 2,
    'ussd_string': '*120*12345#'
}
resp = client.campaigns.post(data=data)
print resp.status_code
print resp.content

created_id = resp.json().get('id')

print
print

resp = client.campaigns.get()
print resp.status_code
print resp.content

print
print

resp = client.campaigns.get(id=created_id)
print resp.status_code
print resp.content

print
print

data = {
    'colony_campaign_id': 1,
    'ussd_string': '*120*123456#'
}

resp = client.campaigns.put(id=created_id, data=data)
print resp.status_code
print resp.content

print
print

resp = client.campaigns.get(id=created_id)
print resp.status_code
print resp.content