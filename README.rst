===============
Colony USSD Pod
===============

The Colony USSD Pod (CUP) API allows you to interact with the CUP REST API.

Installation:
-------------

Install ``cup`` using ``pip``...

   pip install cup -i http://pypi.eurocom.co.za/pypi

Code Example:
-------------

   .. sourcecode:: python

      from cup.client import CUPClient

      client = CUPClient(access_token='xxxxxxxxxx')

      # Create a new USSD screen object.
      data = {
         'campaign_id': 1,
         'ussd_string': '*120*12345#',
         'text': 'Test all the things!',
         'use_vault': True
      }
      resp = client.ussd.post(data=data)

The ``CUPClient`` uses the fantastic Requests_ library. The response that is returned is the same response_ class used
by Requests_ so all the niceties are kept intact.

With the above response we can ge the JSON object as follows.

   .. sourcecode:: python

      obj = resp.json()
      created_id = obj.get('id')

      # Update the USSD screen object we created.
      data = {
         'campaign_id': 1,
         'ussd_string': '*120*12345#',
         'text': 'A brand new text thingy!',
         'use_vault': False
      }
      resp = client.ussd.put(id=created_id, data=data)

As you can see above, when you need to ``PUT``, ``PATCH`` or ``DELETE`` an object you need to pass ``id`` as a kwarg into the
corresponding method call. When you pass the ``id`` kwarg into the ``GET`` call you will be given the single object in the
response.

To view a list of the objects, you can simply call the ``GET`` method.

   .. sourcecode:: python

      # Get all the USSD objects.
      resp = client.ussd.get()

.. _Requests: http://www.python-requests.org/
.. _response: http://www.python-requests.org/en/latest/user/quickstart/#response-content