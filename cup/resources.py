from functools import partial

from requests import Request, Session


class Resource(object):
    """
    An object to work with USSD resources.
    """
    use_ssl = False
    host = '127.0.0.1:8000'
    allowed_methods = ('get', 'post', 'patch', 'put', 'head', 'delete')
    pk_allowed_methods = ('patch', 'put', 'delete')

    def __init__(self, **kwargs):
        """
        Create the resource instance with the provided options. All the options should be passed in as kwargs.
        """
        self.use_ssl = kwargs.get('user_ssl', self.use_ssl)
        self.host = kwargs.get('host', self.host)
        self.name = kwargs.get('name')
        self.object = kwargs.get('object')
        self.scheme = self.use_ssl and 'https://' or 'http://'
        self.access_token = kwargs.get('access_token')
        self.allowed_methods = kwargs.get('allowed_methods', self.allowed_methods)
        self.session = Session()

    def _request(self, method, **kwargs):
        """
        Given an HTTP method, a resource name and kwargs, construct a request and return the response.
        """
        if method in self.pk_allowed_methods and 'id' not in kwargs:
            raise TypeError('When doing %s you need to supply the object\'s id as a '
                            'kwarg.' % ', '.join(self.pk_allowed_methods))

        url = '%(scheme)s%(host)s/%(name)s/%(object)s/' % {
            'scheme': self.scheme,
            'host': self.host,
            'name': self.name,
            'object': self.object
        }
        headers = {'authorization': 'Token %s' % self.access_token}

        if 'id' in kwargs:
            url += '%s/' % kwargs.get('id')

        # Create request.
        req = Request(
            method=method.upper(),
            url=url,
            data=kwargs.get('data', None),
            headers=headers
        )
        prepped_req = self.session.prepare_request(req)

        # Return response.
        return self.session.send(prepped_req)

    def __getattr__(self, name, **kwargs):
        """
        Translate a HTTP verb into a request method.
        """
        if name not in self.allowed_methods:
            raise AttributeError

        return partial(self._request, name, **kwargs)