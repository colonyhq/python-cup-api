from .resources import Resource


class CUPClient(object):
    """
    The API client to connect to the CUP server.
    """
    use_ssl = False
    host = '127.0.0.1:8000'

    def __init__(self, **kwargs):
        """
        Create the client instance with the provided options. All the options should be passed in as kwargs.
        """
        self.use_ssl = kwargs.get('user_ssl', self.use_ssl)
        self.host = kwargs.get('host', self.host)
        self.scheme = self.use_ssl and 'https://' or 'http://'
        self.access_token = kwargs.get('access_token', False)

        if not self.access_token:
            raise TypeError('You need to supply the access_token.')

        # Create the resources.
        # Campaigns Resource
        self.campaigns = Resource(
            use_ssl=self.use_ssl,
            host=self.host,
            name='ussd',
            object='campaigns',
            scheme=self.scheme,
            access_token=self.access_token
        )

        # Modules Resource
        self.modules = Resource(
            use_ssl=self.use_ssl,
            host=self.host,
            name='ussd',
            object='modules',
            scheme=self.scheme,
            access_token=self.access_token
        )

        # Module Links Resource
        self.module_links = Resource(
            use_ssl=self.use_ssl,
            host=self.host,
            name='ussd',
            object='modules/links',
            scheme=self.scheme,
            access_token=self.access_token
        )

        # Dynamic Questions Resource
        self.dynamic_questions = Resource(
            use_ssl=self.use_ssl,
            host=self.host,
            name='ussd',
            object='dynamicquestions',
            scheme=self.scheme,
            access_token=self.access_token
        )